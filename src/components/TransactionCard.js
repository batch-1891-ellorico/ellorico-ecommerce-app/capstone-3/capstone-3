import { Card } from 'react-bootstrap';

export default function TransactionCard({enrollmentsProp}) {
	const {courseId, enrolledOn, status} = enrollmentsProp
	console.log(enrollmentsProp)
	// console.log(enrollmentsProp[1].courseId)

return(
	<Card>
		<Card.Body>
			<Card.Title>Enrolled Subject</Card.Title>
			<Card.Subtitle>Course ID:</Card.Subtitle>
			<Card.Text>{courseId}</Card.Text>
			<Card.Subtitle>Date Enrolled:</Card.Subtitle>
			<Card.Text>{enrolledOn}</Card.Text>
			<Card.Subtitle>Enrollment Status:</Card.Subtitle>
			<Card.Text>{status}</Card.Text>
		</Card.Body>
	</Card>
		)
}