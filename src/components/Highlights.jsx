import {Row, Col, Card, Button} from 'react-bootstrap';

export default function Highlights() {

	return (

			<Row className="mt-3 mb-3">
				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
					  <Card.Body>
					    <Card.Title>Study Digital Media</Card.Title>
					    <Card.Text>
					      Learn basic and advanced digital media concepts and practices from practitioners in the industry.
					    </Card.Text>
					    <Button variant="primary">See Courses</Button>
					  </Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
					  <Card.Body>
					    <Card.Title>Corporate Services</Card.Title>
					    <Card.Text>
					     Resources, Blogs, Tutorials, and others - Free of charge! 
					    </Card.Text>
					    <Button variant="primary">See Services</Button>
					  </Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
					  <Card.Body>
					    <Card.Title>Other Services</Card.Title>
					    <Card.Text>
					      Corporate Services and Careers Information. Consult with us on brand design and let us help you develop the image of your company.  
					    </Card.Text>
					    <Button variant="primary">See Vacancies</Button>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		)
}
