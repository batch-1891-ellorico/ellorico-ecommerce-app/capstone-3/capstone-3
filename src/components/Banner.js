import { Row, Col, Button } from 'react-bootstrap'

export default function Banner () {

	return(
			<Row>
				<Col className="p-4">
					<h1>Impact Studio</h1>
					<p>Enhance your eyes for multimedia arts!</p>
					<Button variant="primary">Enroll Now!</Button>
				</Col>
			</Row>
		)
}
