import { useContext, Fragment } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar () {

  const { user } = useContext(UserContext);

  return (
	<Navbar bg="light" expand="lg">
    	<Container>
        <Navbar.Brand as={ Link } to="/">[ i ]</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto justify-content-end">
            <Nav.Link as={ Link } to="/">HOME</Nav.Link>
            {
              (user.isAdmin !== true) ?
              <Nav.Link as={ Link } to="/courses">COURSES</Nav.Link>
              : 
              <></>
            }
            {
              (user.id !== null && user.isAdmin === true) ?
              <Nav.Link as={ Link } to="/admin">ADMIN PANEL</Nav.Link>
              :
              <></>            
            }
            {
              (user.id !== null && user.isAdmin !== true) ?
              <Nav.Link as={ Link } to="/transactions">TRANSACTIONS</Nav.Link>
              :
              <></>
            }
            {(user.id !== null) ?      
              <Nav.Link as={ Link } to="/logout">LOGOUT</Nav.Link>          
              :
              <Fragment>
              <Nav.Link as={ Link } to="/register">REGISTER</Nav.Link>
              <Nav.Link as={ Link } to="/login">LOGIN</Nav.Link>
              </Fragment>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
		)
}

