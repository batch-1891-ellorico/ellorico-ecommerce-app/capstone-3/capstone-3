import { useState, useEffect } from 'react';
import CourseCard from './CourseCard'

export default function UserView({coursesProp}) {
	console.log(coursesProp)
	const [coursesArr, setCoursesArr] = useState([])
	useEffect(() => {
		const courses = coursesProp.map(course => {
			if(course.isActive){
				return <CourseCard key={course._id} courseProp={course} />
			}else{
				return null
			}
		})
		setCoursesArr(courses)
	}, [coursesProp])
	return(
		<>
			{coursesArr}
		</>
	)
}
