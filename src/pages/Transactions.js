import { Fragment, useState, useEffect } from 'react'
import TransactionCard from '../components/TransactionCard'



export default function Transactions() {

	const [enrollments, setenrollments] = useState([])

	useEffect(() => {
		fetch('https://frozen-basin-32366.herokuapp.com/users/details', {
			method: "GET", 
			headers: {
				'Content-Type': "application/json", 
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}	
		})
		.then(res => res.json())
		.then(data => {
			console.log(data.enrollments)
			setenrollments(data.enrollments.map(enrollments => {
				console.log(enrollments)
				return (
						<TransactionCard key={enrollments._id} enrollmentsProp={enrollments} />
					)
			}))
		})
	}, [])

	return(
			<Fragment>
				<h1>Enrollments</h1>
				{enrollments}
			</Fragment>
		)
}