import { useState, useEffect, useContext } from 'react';

import UserView from  "../components/UserView"
import UserContext from "../UserContext"

export default function Courses() {



	const { user } = useContext(UserContext);
	console.log(user)
	const [coursesData, setCoursesData] = useState([])

	const fetchData = () => {
			fetch('https://frozen-basin-32366.herokuapp.com/courses/')
		.then(response => response.json())
		.then(data => {
	setCoursesData(data)
	console.log(data)
		})
	}
	useEffect(() => {
		fetchData()
	}, [])

	

	return (
		<UserView coursesProp={coursesData}/>
		)

}
