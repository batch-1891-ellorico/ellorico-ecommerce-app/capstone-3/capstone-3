import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import Publications from '../components/Publications';
// import Footer from '../components/Footer';

export default function Home () {
	
	return (
			<>
				<Banner/>
				<Highlights/>
				{/*<Publications/>
				<Footer/>*/}
			</>
		)
}