import { useState, useEffect, useContext } from 'react';

import AdminView from  "../components/AdminView"
import UserContext from "../UserContext"

export default function AdminCourses() {



	const { user } = useContext(UserContext);
	console.log(user)
	const [coursesData, setCoursesData] = useState([])

	const fetchData = () => {
		fetch('https://frozen-basin-32366.herokuapp.com/courses/all', {
		method: "GET", 
		headers: {
			'Content-Type': "application/json", 
			Authorization: `Bearer ${localStorage.getItem('token')}`
			}	
		})
	.then(response => response.json())
	.then(data => {
	setCoursesData(data)
	console.log(data)
		})
	}
	useEffect(() => {
		fetchData()
	}, [])

	

	return (
		<AdminView coursesProp={coursesData} fetchData={fetchData}/>
		)

}
